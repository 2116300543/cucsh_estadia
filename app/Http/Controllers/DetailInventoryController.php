<?php

namespace App\Http\Controllers;

ini_set('max_execution_time', 180);

use Illuminate\Http\Request;
use App\Models\DetailInventory;
use App\Models\Workstation;
use App\Models\WorkstationList;
use App\User;
use App\Models\Area;
use App\Models\Movement;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Input;

class DetailInventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   


       return view('detailsInventory.index');

    }

    public function filtered(Request $request, $id)
    {
        
        if(strlen($id) >= 4){
            $workstations = Workstation::where('udg_id', 'LIKE', "%$id%")->orWhere('id', 'LIKE', "%$id%")->orWhere('numero_serie', 'LIKE', "%$id%")->select('id', 'udg_id', 'numero_serie')->get();
            $detailInventory = DetailInventory::where('IdEquipo', '=', "%$id%")->select('estatus', 'notas')->get();
            
            foreach ($workstations as $key => $workstation) {
                
                if($workstation->id != ""){
                     
                    $movement = Movement::where('id_equipo', '=', $workstation->id)->select('id_area')->first();
                    $user = User::select('id')->first();
                    $list = WorkstationList::find($workstation->id);
                    
    
                    if($movement != null){
    
                        $workstation->movement = $movement->id_area;
                        $workstation->list = $list->unidad;
                        $workstation->user = $user->id;
    
                    }
                }
            }
            return view('detailsInventory.filtered')->with(compact('workstations', 'list', 'movement', 'user'));
            
        }else{
            Alert::error('Datos Incompletos', 'Debe ingresar minimo 4 caracteres.');
            return redirect()->route('detailsInventory.index');
       
    }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */    

    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();
        $detailInventory = new DetailInventory();
        $detailInventory->IdEquipo = $data['IdEquipo'];
        $detailInventory->IdArea = $data['IdArea'];
        $detailInventory->IdRevisor = $data['IdRevisor'];
        $detailInventory->fechaHora = date('Y-m-d H-i-s');
        $detailInventory->inventario = "2020A";
        $detailInventory->estatus = $data['status'];
        $detailInventory->notas = ($data['notes'] != "") ? $data['notes'] : "-" ;
        $detailInventory->save();
        Alert::success('Registro Guardado', 'El Equipo se actualizo con su estatus de forma satisfactoria.');
        return redirect()->route('detailsInventory.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
