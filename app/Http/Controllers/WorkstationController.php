<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Workstation;
use App\Models\WorkstationList;
use App\Models\Movement;
use App\Models\Area;
use App\Models\Employee;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use DataTables;

class WorkstationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('workstations.index');
    }

    public function datatable()
    {
        $list = WorkstationList::all();
        
        return json_encode(array("data" => $list));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $workstation = Workstation::all();
        return view('workstations.create')->with(compact('workstation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'udg_id'        =>        ['required', 'string', 'max:20'],
            'descripcion'   =>        ['required', 'string', 'max:50'],
            'hardwareType'  =>        ['required', 'string', 'max:255'],
            'brand'         =>        ['required', 'string', 'max:20'],
            'model'         =>        ['required', 'string', 'max:50'],
            'serialNumber'  =>        ['required', 'string', 'max:30'],
            'mac'           =>        ['required', 'string', 'max:20'],
            'ip'            =>        ['required', 'string', 'max:16'],
            'conectionType' =>        ['required', 'string', 'max:40'],
            'details'       =>        ['required', 'string', 'max:500'],
            'verified'      =>        ['required', 'string', 'max:40'],
            'frontImage'    =>        ['required', 'string', 'max:30'],
            'serialImage'   =>        ['required', 'string', 'max:15'],
            'invoicePDF'    =>        ['required', 'string', 'max:50'],
            'status'        =>        ['required'],
            'cta'           =>        ['required', 'integer', 'max:4'],
            'safeguardID'   =>        ['required', 'integer', 'max:11'],
            'active'        =>        ['required', 'integer', 'max:1']
        ]);

        if ($validator->fails()) {

            Alert::warning('Campos sin llenar', 'Es necesario llenar los campos.');
            return redirect()->route('workstations.create');

        }

        $data = $request->all();
        $workstation = new Workstation();
        $workstation->udg_id = $data['udg_id'];
        $workstation->descripcion = $data['descripcion'];
        $workstation->tipo_equipo = $data['hardwareType'];
        $workstation->marca = $data['brand'];
        $workstation->modelo = $data['model'];
        $workstation->numero_serie = $data['serialNumber'];
        $workstation->mac = $data['mac'];
        $workstation->ip = $data['ip'];
        $workstation->tipo_conexion = $data['conectionType'];
        $workstation->detalles = $data['details'];
        $workstation->verificado = $data['verified'];
        $workstation->img_frente = $data['frontImage'];
        $workstation->img_serie = $data['serialImage'];
        $workstation->pdf_factura = $data['invoicePDF'];
        $workstation->estado = $data['status'];
        $workstation->cta = $data['cta'];
        $workstation->idResguardante = $data['safeguardID'];
        $workstation->activo = $data['active'];
        $workstation->save();
        Alert::success('Registro creado', 'El equipo se dio de alta satisfactoriamente');
        return redirect()->route('workstations.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $workstation = Workstation::find($id);
      
        return view('workstations.edit')->with(compact('workstation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $data = $request->all();
        $workstation = Workstation::find($id);
        $workstation->udg_id = $data['udg_id'];
        $workstation->descripcion = $data['descripcion'];
        $workstation->tipo_equipo = $data['hardwareType'];
        $workstation->marca = $data['brand'];
        $workstation->modelo = $data['model'];
        $workstation->numero_serie = $data['serialNumber'];
        $workstation->mac = $data['mac'];
        $workstation->ip = $data['ip'];
        $workstation->tipo_conexion = $data['conectionType'];
        $workstation->detalles = $data['details'];
        $workstation->verificado = $data['verified'];
        $workstation->img_frente = $data['frontImage'];
        $workstation->img_serie = $data['serialImage'];
        $workstation->pdf_factura = $data['invoicePDF'];
        $workstation->estado = $data['status'];
        $workstation->cta = $data['cta'];
        $workstation->idResguardante = $data['safeguardID'];
        $workstation->activo = $data['active'];
        $workstation->update();
        Alert::success('Registro modificado', 'El equipo se actualizo satisfactoriamente');
        return redirect()->route('workstations.index');
    }

    public function assign(){
        return view('workstations.assign');
    }

    public function save(Request $request){
        $data = $request->all();
        $workstation = Workstation::select('id', 'IdResguardante')->where('id', '=',  $data['id_equipo'])->first();
      
        $movement = new Movement();
        $movement->id_equipo = $data['id_equipo'];
        $movement->id_area = (int)$data['id_area'];
        $movement->id_usuario = (int)$data['id_usuario'];
        $movement->id_resguardante = $workstation->IdResguardante;
        $movement->registro = $data['registro'];
        $movement->fecha_hora = date('Y-m-d H-i-s');
        $movement->comentarios = ($data['comentarios'] != "") ? $data['comentarios'] : "-" ;
        $movement->save();
        Alert::success('Registro Asignado', 'El equipo se asigno a un empleado satisfactoriamente');
        return redirect()->route('workstations.index');
    }

    public function assignArea(){

        $queries = Area::select('id', 'sede', 'edificio', 'division', 'unidad')->get();

        $count = 0;

        foreach ($queries as $query)
        {
          
            $results[$count]['id'] = $query->id;
            $results[$count]['value'] =  $query->sede.' - '.$query->edificio.' - '.$query->division.' - '.$query->unidad ;
            $count++;

        }
    
        return response()->json($results);
    }

    public function assignUser(){

        $queries = Employee::select('id', 'nombre')->get();

        $count = 0;

        foreach ($queries as $query)
        {
            $results[$count]['id'] = $query->id;
            $results[$count]['value'] =  $query->nombre ;
            $count++;

        }
        return response()->json($results);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $workstation = Workstation::find($id);
        $workstation->delete();
        
        
        Alert::success('Registro eliminado', 'El equipo se dio de alta satisfactoriamente');
        return redirect()->route('workstations.index');
    }
}
