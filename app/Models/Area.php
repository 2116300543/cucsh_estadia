<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    
    protected $table = 'areas';
    protected $fillable = ['tipoEspacio', 'sede', 'edificio', 'piso', 'division', 
                          'coordinacion', 'unidad', 'activo', 'tipo'];

    public $timestamps = false;

    public function detailInventory()
    {
        return $this->belongsTo('App\Models\DetailInventory');
    }

    public function movement()
    {
        return $this->belongsTo('App\Models\Movement');
    }
}
