<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailInventory extends Model
{
    protected $table = 'inventariodetalle';
    protected $fillable = ['IdEquipo', 'IdArea', 'IdRevisor', 'fechaHora', 
                          'inventario', 'estatus', 'notas', 'created_at', 'updated_at'];


    public function workstation()
    {
        return $this->hasOne('App\Models\Workstation');
    }
        
    public function area()
    {
        return $this->hasOne('App\Models\Area');
    }

    public function user()
    {
        return $this->hasOne('App\User');
    }
}
