<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'empleados';
    protected $fillable = ['area_id', 'codigo_udg', 'nombre', 'puesto', 
                          'grado_estudios', 'registro', 'observaciones', 'extension', 'activo'];

    public $timestamps = false;

    public function workstation()
    {
        return $this->hasMany('App\Models\Workstation');
    }
}
