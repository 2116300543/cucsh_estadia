<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{
    protected $table = 'movimientos_equipos';
    protected $fillable = ['id_movimiento', 'id_equipo', 'id_area', 'id_usuario', 
                          'id_resguardante', 'registro', 'fecha_hora', 'comentarios'];
    protected $primaryKey = 'id_movimiento';

    public $timestamps = false;

    public function area()
    {
        return $this->hasOne('App\Models\Area');
    }

    public function workstation()
    {
        return $this->hasOne('App\Models\Workstation');
    }
}
