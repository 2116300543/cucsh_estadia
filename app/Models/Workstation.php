<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workstation extends Model
{

    protected $table = 'Equipos';
    protected $fillable = ['udg_id', 'descripcion', 'tipo_equipo', 'marca', 'modelo', 
                          'numero_serie', 'mac', 'ip', 'tipo_conexion', 'detalles', 'verificado', 
                          'img_frente', 'img_serie', 'pdf_factura', 'estado', 'cta', 'IdResguardante', 'activo'];
    public $timestamps = false;

    public function workstationList()
    {
        return $this->belongsTo('App\Models\WorkstationList');
    }

    public function detailInventory()
    {
        return $this->belongsTo('App\Models\DetailInventory');
    }
    public function movement()
    {
        return $this->belongsTo('App\Models\Movement');
    }
    public function employee()
    {
        return $this->belongsTo('App\Models\Employee');
    }
}
