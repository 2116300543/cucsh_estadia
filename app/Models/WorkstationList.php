<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkstationList extends Model
{
    protected $table = 'lista_equipos';
    protected $fillable = ['udg_id', 'tipo_equipo', 'marca', 'modelo', 
                          'numero_serie', 'detalles', 'estado', 'id_area', 'sede', 'division', 
                          'coordinacion', 'unidad'];

    public $timestamps = false;

    public function workstation()
    {
        return $this->hasOne('App\Models\Workstation');
    }
}
