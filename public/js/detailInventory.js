jQuery(document).ready(function($) {

    $.each($('.workstation td input'), function(index, item) {
        var workstationID = $(item).val();

        $('#status_' + workstationID).on('change', function() {
            if (this.value == "No localizado") {
                $('#notes_' + workstationID).show();
                $('#notesL_' + workstationID).show();
                $('#notesM_' + workstationID).show();
            } else {
                $('#notes_' + workstationID).hide();
                $('#notesL_' + workstationID).hide();
                $('#notesM_' + workstationID).hide();
            }

        });
    });


    $('#search').on('click', function() {
        var id = $("#idWorkstation").val();

        search(id)

    })

    function search(id) {
        if (id === '') {

            swal({
                title: "Campo vacio",
                position: "top-center",
                text: "No ingreso ningun dato para buscar.",
                icon: "error"

            })
        } else {

            window.location = "detailsInventory/" + id + "/filtered";

        }

    }

});