@extends('layouts.main')
@section('title', 'Equipos')

@section('content')
        <div class="card">
          <div class="card-header bg-light text-bl">
            Asignación del equipo
            <a class="btn btn-danger float-right text-white" href="{{route('workstations.index')}}">Volver</a>
            
          </div> 
          <br>
          <br>
          <br>
          <div class="col-12">
            <form class="form-mequipos row justify-content-center" method="POST" action="{{route('workstations.save')}}">
                @method('POST')
                @csrf
            <input type="hidden" name="id_equipo" value="{{Request::segment(2)}}">
                <div class="col-md-6 col-sm-12">
                    <!-- linea agregada -->
                    <div id="keyAreas" class="form-group dropdow-options">
                        <label class="font-weight-bold">Área</label>
                        <input type="hidden" name="id_area" id="areaId">
                        <input class="form-control" type="text" name="area" id="areaID"  placeholder="Buscar...">
                        
                    </div>
    
    
                    <div id="keyUsuario" class="form-group dropdow-options">
                        <label class="font-weight-bold">Usuario</label>
                        <input type="hidden" name="id_usuario" id="usuarioId">
                        <input class="form-control" type="text" name="usuario" id="usuarioID" placeholder="Buscar...">
                   
                    </div>
    
                    <!-- linea agregada -->
                
    
                    <div class="form-group">
                        <label class="font-weight-bold">Registro</label>
                        <textarea name="registro" class="form-control">Registro Inicial en la Base de Datos</textarea>
                    </div>
    
                    <div class="form-group">
                        <label class="font-weight-bold">Fecha</label>
                        <input type="date" class="form-control" >
                    </div>
    
                    <div class="form-group">
                        <label class="font-weight-bold">Comentarios</label>
                        <textarea name="comentarios" class="form-control"> -</textarea>
                    </div>
    
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-success">Guardar</button>
                </div>
            </form>
          </div>
          </div> 

       <script type="text/javascript">
            $(document).ready(function() { 
            $.noConflict();
                $('#areaID').autocomplete({
                    source: "{{route('workstations.assignArea')}}",
                    select: function(event, ui) { 
                        $("#areaId").val(ui.item.id) 
                    }
                });

                $('#usuarioID').autocomplete({
                    source: "{{route('workstations.assignUser')}}",
                    select: function(event, ui) { 
                        $("#usuarioId").val(ui.item.id) 
                    }
                });
            }); 
       </script>
@endsection
