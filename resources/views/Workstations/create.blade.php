@extends('layouts.main')
@section('title', 'Registrar Equipo')
@section('content')
<form method="POST" action="{{route('workstations.store')}}">
  @method('POST')
  @csrf
  <div class="card">
    <div class="card-header bg-dark text-white">
        Registro de equipo
    </div>
    <div class="card-body">
        @include('workstations.form')
    </div>
        <div class="card-footer text-muted text-right">
            <a class="btn btn-danger" href="{{route('workstations.index')}}">Volver al listado</a>
            <button type="submit" class="btn btn-success">Guardar</button>
    </div>
  </div>
</form>
@endsection