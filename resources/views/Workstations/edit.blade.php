@extends('layouts.main')
@section('title', 'Modificar Equipos')
@section('content')

<form method="POST" action="{{route('workstations.update', $workstation->id)}}">
  @method('PUT')
  @csrf
  <div class="card">
    <div class="card-header bg-dark text-white">
    Modificar Equipo
    </div>
    <div class="card-body">
        @include('workstations.form')
    </div>
        <div class="card-footer text-muted text-right">
            <a class="btn btn-danger" href="{{route('workstations.index')}}">Cancelar</a>
            <button type="submit" class="btn btn-warning">Actualizar</button>
    </div>
  </div>
</form>
@endsection