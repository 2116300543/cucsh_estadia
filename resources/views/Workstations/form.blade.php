<div class="row">
    <div class="col-md-4">
     <div class="form-group">
         <label for="udg_id"><strong>ID de UDG</strong></label>
     <input type="text" class="form-control" id="udg_id" maxlength="20" name="udg_id" aria-describedby="udg_idHelp" value="{{( isset($workstation->udg_id) ? $workstation->udg_id : old('udg_id'))}}">
         <small id="udg_idHelp" class="form-text text-muted">ID de UDG</small>
         @if ($errors->has('udg_id'))
            <small class="text-danger">{{$errors->first('udg_id')}}</small>
         @endif
       </div>
    </div>
        <div class="col-md-4">
         <div class="form-group">
             <label for="descripcion"><strong>Descripción del Equipo</strong></label>
         <input type="text" class="form-control" maxlength="50" id="descripcion" name="descripcion" aria-describedby="descripcionHelp" value="{{( isset($workstation->descripcion) ? $workstation->descripcion : old('descripcion'))}}">
             <small id="descripcionHelp" class="form-text text-muted">Descripción del equipo.</small>
             @if ($errors->has('descripcion'))
                <small class="text-danger">{{$errors->first('descripcion')}}</small>
             @endif
           </div>
        </div>
     <div class="col-md-4">
         <div class="form-group">
             <label for="hardwareType"><strong>Tipo de Equipo</strong></label>
             <input type="text" class="form-control" id="hardwareType" name="hardwareType" aria-describedby="hardwareTypeHelp" value="{{( isset($workstation->tipo_equipo) ? $workstation->tipo_equipo : old('tipo_equipo'))}}">
             <small id="hardwareTypeHelp" class="form-text text-muted">Tipo de equipo.</small>
             @if ($errors->has('hardwareType'))
                 <small class="text-danger">{{$errors->first('hardwareType')}}</small>
             @endif
        </div>
     </div>
     <div class="col-md-4">
        <div class="form-group">
            <label for="brand"><strong>Marca del Equipo</strong></label>
            <input type="text" class="form-control" maxlength="20" id="brand" name="brand" aria-describedby="brandHelp" value="{{( isset($workstation->marca) ? $workstation->marca : old('marca'))}}">
            <small id="brandHelp" class="form-text text-muted">Marca del equipo.</small>
            @if ($errors->has('brand'))
                <small class="text-danger">{{$errors->first('brand')}}</small>
            @endif
          </div>
        </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="model"><strong>Modelo del Equipo</strong></label>
            <input type="text" class="form-control" maxlength="50" id="model" name="model" aria-describedby="modelHelp" value="{{( isset($workstation->modelo) ? $workstation->modelo : old('modelo'))}}">
            <small id="modelHelp" class="form-text text-muted">Modelo del equipo.</small>
            @if ($errors->has('model'))
                <small class="text-danger">{{$errors->first('model')}}</small>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="serialNumber"><strong>Número de Serie</strong></label>
            <input type="text" class="form-control" maxlength=""30 id="serialNumber" name="serialNumber" aria-describedby="serialNumberHelp" value="{{( isset($workstation->numero_serie) ? $workstation->numero_serie : old('numero_serie'))}}">
            <small id="serialNumberHelp" class="form-text text-muted">Número de serie.</small>
            @if ($errors->has('serialNumber'))
                <small class="text-danger">{{$errors->first('serialNumber')}}</small>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="mac"><strong>Mac</strong></label>
            <input type="text" class="form-control" maxlength="20" id="mac" name="mac" aria-describedby="macHelp" value="{{( isset($workstation->mac) ? $workstation->mac : old('mac'))}}">
            <small id="macHelp" class="form-text text-muted">Mac.</small>
            @if ($errors->has('mac'))
                <small class="text-danger">{{$errors->first('mac')}}</small>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="ip"><strong>IP</strong></label>
            <input type="text" class="form-control" maxlength="16" id="ip" name="ip" aria-describedby="ipHelp" value="{{( isset($workstation->ip) ? $workstation->ip : old('ip'))}}">
            <small id="ipHelp" class="form-text text-muted">IP del equipo.</small>
            @if ($errors->has('ip'))
                <small class="text-danger">{{$errors->first('ip')}}</small>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="conectionType"><strong>Tipo de Conexión</strong></label>
            <input type="text" class="form-control" maxlength="40" id="conectionType" name="conectionType" aria-describedby="conectionTypeHelp" value="{{( isset($workstation->tipo_conexion) ? $workstation->tipo_conexion : old('tipo_conexion'))}}">
            <small id="conectionTypeHelp" class="form-text text-muted">Tipo de conexión del equipo.</small>
            @if ($errors->has('conectionType'))
                <small class="text-danger">{{$errors->first('conectionType')}}</small>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="details"><strong>Detalles del Equipo</strong></label>
            <input type="text" class="form-control" id="details" name="details" aria-describedby="detailsHelp" value="{{( isset($workstation->detalles) ? $workstation->detalles : old('detalles'))}}">
            <small id="detailsHelp" class="form-text text-muted">Detalles del equipo.</small>
            @if ($errors->has('details'))
                <small class="text-danger">{{$errors->first('details')}}</small>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="verified"><strong>Verificado</strong></label>
            <input type="text" class="form-control" maxlength="40" id="verified" name="verified" aria-describedby="verifiedHelp" value="{{( isset($workstation->verificado) ? $workstation->verificado : old('verificado'))}}">
            <small id="verifiedHelp" class="form-text text-muted">Verificado.</small>
            @if ($errors->has('verified'))
                <small class="text-danger">{{$errors->first('verified')}}</small>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="frontImage"><strong>Imagen de Frente</strong></label>
            <input type="text" class="form-control" maxlength="30" id="frontImage" name="frontImage" aria-describedby="frontImageHelp" value="{{( isset($workstation->img_frente) ? $workstation->img_frente : old('img_frente'))}}">
            <small id="frontImageHelp" class="form-text text-muted">Imagen de frente.</small>
            @if ($errors->has('frontImage'))
                <small class="text-danger">{{$errors->first('frontImage')}}</small>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="serialImage"><strong>Imagen de Serie</strong></label>
            <input type="text" class="form-control" maxlength="15" id="serialImage" name="serialImage" aria-describedby="serialImageHelp" value="{{( isset($workstation->img_serie) ? $workstation->img_serie : old('img_serie'))}}">
            <small id="serialImageHelp" class="form-text text-muted">Imagen de serie.</small>
            @if ($errors->has('serialImage'))
                <small class="text-danger">{{$errors->first('serialImage')}}</small>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="invoicePDF"><strong>PDF de factura</strong></label>
            <input type="text" class="form-control" maxlength="50" id="invoicePDF" name="invoicePDF" aria-describedby="invoicePDFHelp" value="{{( isset($workstation->pdf_factura) ? $workstation->pdf_factura : old('pdf_factura'))}}">
            <small id="invoicePDFHelp" class="form-text text-muted">PDF de factura.</small>
            @if ($errors->has('invoicePDF'))
                <small class="text-danger">{{$errors->first('invoicePDF')}}</small>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="status"><strong>Seleccione estado del Equipo</strong></label>
            <select class="form-control" id="status" name="status" aria-describedby="statusHelp">
                <option value="-1">Seleccione el Estado</option>
                <option value="Activo"  {{ (isset($workstation->id ) && 'Activo' == $workstation->estado)  ? 'selected': old('estado')}}>Activo</option>
                <option value="No Activo"  {{ (isset($workstation->id ) && 'No Activo' == $workstation->estado ) ? 'selected': old('estado')}}>No Activo</option>
                <option value="Baja"  {{ (isset($workstation->id ) && 'Baja' == $workstation->estado ) ? 'selected': old('estado')}}>Baja</option>
            </select>
            <small id="statusHelp" class="form-text text-muted">Seleccione estado del equipo.</small>
            @if ($errors->has('status'))
               <small class="text-danger">{{$errors->first('status')}}</small>
           @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="cta"><strong>CTA</strong></label>
            <input type="number" class="form-control" id="cta" max="4" name="cta" aria-describedby="ctaHelp" value="{{( isset($workstation->cta) ? $workstation->cta : old('cta'))}}">
            <small id="ctaHelp" class="form-text text-muted">CTA.</small>
            @if ($errors->has('cta'))
                <small class="text-danger">{{$errors->first('cta')}}</small>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="safeguardID"><strong>ID Resguardante</strong></label>
            <input type="number" class="form-control" max="11" id="safeguardID" name="safeguardID" aria-describedby="safeguardIDHelp" value="{{( isset($workstation->IdResguardante) ? $workstation->IdResguardante : old('IdResguardante'))}}">
            <small id="safeguardIDHelp" class="form-text text-muted">ID Resguardante.</small>
            @if ($errors->has('safeguardID'))
                <small class="text-danger">{{$errors->first('safeguardID')}}</small>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="active"><strong>Activo</strong></label>
            <input type="number" class="form-control" max="1" id="active" name="active" aria-describedby="activeHelp" value="{{( isset($workstation->activo) ? $workstation->activo : old('activo'))}}">
            <small id="activeHelp" class="form-text text-muted">Activo.</small>
            @if ($errors->has('active'))
                <small class="text-danger">{{$errors->first('active')}}</small>
            @endif
        </div>
    </div>
</div>