@extends('layouts.main')
@section('title', 'Equipos')

@section('content')
        <div class="card">
          <div class="card-header bg-light text-bl">
              Lista de Equipos
              <a class="btn float-right text-white" style="background-color: #9a3616" href="{{route('workstations.create')}}"><i class="fas fa-plus-square">&nbsp;</i>Nuevo registro</a>
          </div> 
          <div class="card-body">
             <div class="row">
                <div class="col-12">
                    <table id="workstation_datatable" class="table table-sm table-bordered" style="width:100%">
                      <thead class="tableHeader">
                        <tr>
                          <th class="text-center tablehead">ID</th>
                          <th class="text-center tablehead">Tipo de equipo</th>
                          <th class="text-center tablehead">ID de UDG</th>
                          <th class="text-center tablehead">Marca</th>
                          <th class="text-center tablehead">Modelo</th>
                          <th class="text-center tablehead">Número de<br>Serie</th>
                          <th class="text-center tablehead" style="width: 60%">Descripción</th>
                          <th class="text-center tablehead" style="width: 70%">Ubicación</th>
                          <th class="text-center tablehead" colspan="2" style="width: 10%">Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                            
                      </tbody>
                    </table>
                </div>
             </div>
          </div>
        </div>
        <script type="text/javascript">

            let edit_route = '{{route('workstations.edit', "_ID_")}}';
            let destroy_route = '{{route('workstations.destroy', "_ID_")}}';
            let assign_route = '{{route('workstations.assign', "_ID_")}}';

              $(document).ready(function() {
                  let language_datatable = {
                  "decimal":        "",
                  "emptyTable":     "No hay registros",
                  "info":           "Mostrando _START_ de _END_ de _TOTAL_ entradas",
                  "infoEmpty":      "Mostrando 0 de 0 de 0 entradas",
                  "infoFiltered":   "(Filtro de _MAX_ entradas totales)",
                  "infoPostFix":    "",
                  "thousands":      ",",
                  "lengthMenu":     "Mostrar _MENU_ entradas",
                  "loadingRecords": "Cargando...",
                  "processing":     "Procesando...",
                  "search":         "Buscar:",
                  "zeroRecords":    "No existen registros con esos valores",
                  "paginate": {
                      "first":      "Primera",
                      "last":       "Ultima",
                      "next":       "Siguiente",
                      "previous":   "Anterior"
                  },
                  "aria": {
                      "sortAscending":  ": activate to sort column ascending",
                      "sortDescending": ": activate to sort column descending"
                  }
              }
                $.noConflict();
                $("#workstation_datatable").DataTable({
                  "ajax": {
                    "url":  "{{route('workstations.datatable')}}",
                    "type": "GET"
                  },
                  language: language_datatable,
                  columns: [
                    {data: "id"},
                    {data: "tipo_equipo"},
                    {data: "udg_id"},
                    {data: "marca"},
                    {data: "modelo"},
                    {data: "numero_serie"},
                    {data: "detalles"},
                    {data: "unidad"}
                    ],
                    "aoColumnDefs": [ 
                      { "aTargets": [8], 
                        "mData": null, "mRender": function (data, type, full) 
                        { 
                            $btn = '<a id="update" tittle="Ver más" href="' + edit_route.replace("_ID_", data.id) + '" class="btn btn-link text-warning"><i class="fas fa-pencil-alt"></i></a>';
                            $btn2 = '<form id="destroy" action="' + destroy_route.replace("_ID_", data.id) + '" method="post">  @method('DELETE') @csrf <button type="submit" class="btn btn-link text-danger"><i class="fas fa-trash"></i></button></form>';
                            $btn3 = '<a id="more" href="' + "" + '" class="btn btn-link text-info"><i class="fas fa-info"></i></a>';
                            $btn4 = '<a id="assignWs" href="' + assign_route.replace("_ID_", data.id) + '" class="btn btn-link text-primary"><i class="fas fa-address-book"></i></a>';
                           
                          return  $btn3.concat($btn, $btn2, $btn4)
                        }
                      } 
                      ] 
                });
            });
        </script>
@endsection
