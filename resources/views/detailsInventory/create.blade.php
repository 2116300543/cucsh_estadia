@extends('layouts.main')
@section('title', 'Inventario')

@section('content')
<div class="card">
        <div class="card-header bg-light text-bl">
          Equipo
        </div> 
        <div class="card-body">
          <div class="row">
              <div class="col-12">
                <table class="table table-sm table-bordered text-center" style="width:100%">
                  <thead class="tableHeader">
                    <tr>
                      <th class="text-center tablehead">ID del Equipo</th>
                      <th class="text-center tablehead">ID de UDG</th>
                      <th class="text-center tablehead">Número de<br>Serie</th>
                      <th class="text-center tablehead">Ubicación</th>
                      <th class="text-center tablehead">Estatus</th>
                      <th class="text-center tablehead">Nota</th>
                    </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>{{$workstation['id']}}</td>
                          <td>{{$workstation['udg_id']}}</td>
                          <td>{{$workstation['numero_serie']}}</td>
                          <td>{{ (isset($workstation->detailInventory) ? $workstation['detailInventory']['IdArea'].' - '.$workstation['area']['sede'] : '-') }}</td>
                          <td>{{ (isset($workstation->detailInventory) ? $workstation['detailInventory']['estatus'] : '-') }}</td>
                          <td>{{ (isset($workstation->detailInventory) ? $workstation['detailInventory']['notas'] : '-') }}</td>
                      </tr>
                  </tbody>
                </table>
              </div>
@endsection