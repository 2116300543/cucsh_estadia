@extends('layouts.main')
@section('title', 'Inventarios')

@section('content')
  <div class="card">
    <div class="card-header bg-light text-bl">
      Equipo
    </div> 
    <div class="card-body">
      <div class="row">
          <div class="col-12">
            <table class="table table-sm table-bordered text-center" style="width:100%">
            <tbody class="workstation" >
                    @foreach ($workstations as $workstation)
                      <thead class="tableHeader">
                        <tr>
                          <th class="text-center tablehead">ID del Equipo</th>
                          <th class="text-center tablehead">ID de UDG</th>
                          <th class="text-center tablehead">Número de<br>Serie</th>
                          <th class="text-center tablehead">ID de Area</th>
                          <th class="text-center tablehead" style="display: none;">Revisor</th>
                          <th class="text-center tablehead" style="width: 20%;">Ubicación</th>
                          <th class="text-center tablehead">Estatus</th>
                          <th class="text-center tablehead">Nota</th>
                        </tr>
                      </thead>
                        <tr class="workstation">
                          <td><input type="hidden" class="workstation_id" value="{{$workstation['id']}}">{{$workstation['id']}}</td>
                          <td>{{$workstation['udg_id']}}</td>
                          <td>{{$workstation['numero_serie']}}</td>
                          <td>{{$workstation->movement}}</td>
                          <td style="display: none;" value="{{$workstation->user}}">{{$workstation->user}}</td>
                          <td>{{$workstation->list}}</td>
                          <td>{{(isset($workstation->detailInventory) ? $workstation['detailInventory']['estatus'] : '-') }}</td>
                          <td>{{(isset($workstation->detailInventory) ? $workstation['detailInventory']['notas'] : '-') }}</td>
                        </tr>
                    <tr>
                      <td colspan="3">
                        @if (!isset($workstation->detailInventory))
                            <form method="POST" action="{{route('detailsInventory.store')}}">
                                  @method('POST')
                                  @csrf
                                    <input type="hidden" value="{{$workstation['id']}}" name="IdEquipo">
                                    <input type="hidden" value="{{$workstation->movement}}" name="IdArea">
                                    <input type="hidden" value="{{$workstation->user}}" name="IdRevisor">
                                    <div class="col-md-8">
                                      <div class="form-group">
                                        <label for="status"><strong>Estatus del equipo</strong></label>
                                      <select class="form-control" id="status_{{$workstation['id']}}" name="status" aria-describedby="statusHelp" >
                                          <option value="-1" disabled selected>Seleccione el estatus</option>
                                          <option selected="true" value="Localizado">Revisado</option>
                                          <option value="No localizado">Revisado con comentario</option>
                                          <option value="Revision">En revisión</option>
                                      </select>      
                                    <td colspan="4">
                                      <div class="col-md-8">
                                        <div class="form-group">
                                          <label id="notesL_{{$workstation['id']}}" style="display: none;" for="notes"><strong>Nota a agregar</strong></label>
                                          <input type="text" style="display: none;" class="form-control" maxlength="30" id="notes_{{$workstation['id']}}" name="notes" value="" aria-describedby="notes">
                                          <small id="notesM_{{$workstation['id']}}"  style="display: none;" class="form-text text-muted">Ingrese el comentario que desee guardar</small>
                                        </div>
                                      </div>
                                    </td> 
                                  
                                    @else 
                                    <div class="card-footer text-muted text-right">
                                      <a class="btn btn-danger" href="{{route('detailsInventory.index')}}">Volver</a>
                                    </div>
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td colspan="7">
                          <div class="card-footer text-muted text-right">
                            <a class="btn btn-danger" href="{{route('detailsInventory.index')}}">Cancelar</a>
                          <button type="submit" id="save" class="btn btn-success">Revisado</button>
                        </div>
                        </td>
                      </tr>
                    </form>
                  @endforeach
              </tbody>
            </table>
          </div>
    
  <script type="text/javascript" src="{{asset('js\detailInventory.js')}}"></script>
@endsection