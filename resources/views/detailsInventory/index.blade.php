@extends('layouts.main')
@section('title', 'Inventario')

@section('content')
            <div class="card">
                <div class="card-header bg-light text-bl">
                    Busqueda de Equipo
                </div> 
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 ">
                        <div class="row">
                                <div class="form-group">
                                    <label id="idWorkstations"for="idWorkstation"><strong>Ingrese el dato que desea buscar</strong></label>
                                    <input type="text"  class="form-control" minlength="4" id="idWorkstation" name="idWorkstation" aria-describedby="idWorkstationHelp">
                                    <small id="idWorkstationss" id="idWorkstationHelp" class="form-text text-muted">ID del equipo a buscar.</small>
                                </div>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="form-group">
                                <label for=""> </label>
                                <div class="text-muted text-center">
                                    <a class="btn btn-success text-white"  id="search"><i class="fas fa-search">&nbsp;</i>Buscar</a>
                            </div> 
                        </div>
                            
                    </div>
                </div>
              </div>
             
          </div>
          <script type="text/javascript" src="{{asset('js\detailInventory.js')}}"></script>
@endsection