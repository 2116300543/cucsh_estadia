<nav class="navbar navbar-expand-lg navbar-dark">
  <a class="navbar-brand" href="/">CUCSH</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item {{ Route::is('home') ? 'active' : ''}}">
      <a class="nav-link" href="/">Inicio </a>
      </li>
      <ul class="nav navbar-nav navbar-right">
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" {{ Route::is('workstations.index') ? 'active' : ''}} id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Equipos
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{route('workstations.index')}}">Lista de Equipos</a>
            <a class="dropdown-item" href="#">Movimientos de Equipos</a>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" {{ Route::is('workstations.index') ? 'active' : ''}} id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Inventarios
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{route('detailsInventory.index')}}">Busqueda de equipos</a>
        </li>
      </ul>
    </ul>
      </div>
    

  </div>
</nav>