<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('workstations/save', 'WorkstationController@save')->name('workstations.save');
Route::post('workstations/store', 'WorkstationController@store')->name('workstations.store');

Route::resource('workstations', 'WorkstationController');
Route::resource('detailsInventory', 'DetailInventoryController');


Route::get('workstations/{workstation}/assign', 'WorkstationController@assign')->name('workstations.assign');
Route::get('detailsInventory/{detailsInventory}/filtered', 'DetailInventoryController@filtered')->name('detailsInventory.filtered');
